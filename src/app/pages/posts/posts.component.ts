import { Component, OnInit } from '@angular/core';
import { Post } from '../../models';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts: Post[] = [];

  constructor() { }

  ngOnInit() {
    for (let i = 1; i <= 5; ++i) {
      this.posts.push({
        id: i,
        cover: `/assets/images/pics/00${i}.jpg`,
        title: `title ${i}`,
        desc: `The theme file should not be imported into other SCSS files. This will cause duplicate styles to be written into your CSS output. If you want to consume the theme definition object (e.g., $candy-app-theme) in other SCSS files, then the definition of the theme object should be broken into its own file, separate from the inclusion of the mat-core and angular-material-theme mixins.`,
        content: `content ${i}`
      });
    }
  }
}
