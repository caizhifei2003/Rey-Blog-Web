import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';

import { MatToolbarModule, MatIconModule } from '@angular/material';

import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  imports: [
    FlexLayoutModule,
    RouterModule,
    MatToolbarModule,
    MatIconModule
  ],
  declarations: [
    NavbarComponent
  ],
  exports: [
    NavbarComponent
  ]
})
export class NavigationModule { }
