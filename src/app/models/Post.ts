export class Post {
    id: number;
    cover: string;
    title: string;
    desc: string;
    content: string;
}